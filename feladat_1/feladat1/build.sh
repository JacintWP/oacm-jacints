#!/bin/bash

CC=g++
SOURCES=`find src -type f -name "*.cpp"`
BIN=example
FLAGS="-std=c++14 -fPIC"

$CC $SOURCES $FLAGS -o $BIN -I src -I src/ga
