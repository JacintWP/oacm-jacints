#pragma once

#include <iostream>

class Welcome {
	public:
		explicit Welcome(std::string name);

		void greeting();

	protected:
		std::string name;
};
