#pragma once

#include <string>

namespace logger
{

class Logger {
	public:
		enum class LEVEL {
				DUMP = 0,
				DEBUG = 1,
				INFO = 2,
				WARNING = 3,
				ERROR = 4
		};

	public:
		static void initialize(LEVEL level);
		static void deinitialize();
		static void l(LEVEL level, const std::string &message);

	protected:
		static std::string levelToString(LEVEL level);

	protected:
		static LEVEL level;
};

} // namespace logger
