#include "logger.h"

#include <iostream>
#include <chrono>
#include <ctime>

using namespace logger;

Logger::LEVEL Logger::level = Logger::LEVEL::INFO;

void Logger::initialize(LEVEL level)
{
	Logger::level = level;

	std::chrono::system_clock::time_point timePoint = std::chrono::system_clock::now();
	std::time_t now = std::chrono::system_clock::to_time_t(timePoint);

	l(LEVEL::INFO, "=========================================================================");
	l(LEVEL::INFO, " EXAMPLE log 1.0");
	l(LEVEL::INFO, std::string(" Created at: ") + std::ctime(&now) +"");
	l(LEVEL::INFO, "-------------------------------------------------------------------------");
}

void Logger::deinitialize()
{
}

void Logger::l(Logger::LEVEL level, const std::string &message)
{
	if (level < Logger::level) {
		return;
	}

	std::cout << "[" << levelToString(level) << "] " << message << std::endl;
}

std::string Logger::levelToString(Logger::LEVEL level) {
	switch(level) {
		case Logger::LEVEL::DUMP: return "DUMP   ";
		case Logger::LEVEL::DEBUG: return "DEBUG  ";
		case Logger::LEVEL::INFO: return "INFO   ";
		case Logger::LEVEL::WARNING: return "WARNING";
		case Logger::LEVEL::ERROR: return "ERROR  ";
		default: return "UNKNOWN";
	}
}
