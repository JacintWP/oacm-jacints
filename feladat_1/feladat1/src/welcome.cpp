#include "welcome.h"

#include <logger/logger.h>

Welcome::Welcome(std::string name)
	: name(std::move(name))
{
	logger::Logger::l(logger::Logger::LEVEL::INFO, "Welcome initialized with: " + this->name);
}

void Welcome::greeting()
{
	std::cout << "Hello " << name << "." << std::endl;
}
